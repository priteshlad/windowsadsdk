﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MASTAdView;
using Windows.UI.Core;
using Windows.ApplicationModel.Core;
using MASTNativeAdView;

namespace PubAdSample.AdFormats
{
    public sealed partial class Native : Page
    {
        private String AD_URL = "http://ads.mocean.mobi/ad";

        public Native()
        {
            this.InitializeComponent();
        }

        private List<AssetRequest> getAssetRequests()
        {
            List<AssetRequest> assets = new List<AssetRequest>();

            TitleAssetRequest titleAsset = new TitleAssetRequest();
            titleAsset.setAssetId(1); // Unique assetId is mandatory for each asset
            titleAsset.setLength(50);
            titleAsset.setRequired(true); // Optional (Default: false)

            assets.Add(titleAsset);

            ImageAssetRequest imageAssetIcon = new ImageAssetRequest();
            imageAssetIcon.setAssetId(2);
            imageAssetIcon.setImageType(ImageAssetTypes.icon);
            imageAssetIcon.setWidth(60); // Optional
            imageAssetIcon.setHeight(60); // Optional

            assets.Add(imageAssetIcon);

            ImageAssetRequest imageAssetLogo = new ImageAssetRequest();
            imageAssetLogo.setAssetId(3);
            imageAssetLogo.setImageType(ImageAssetTypes.logo);
            assets.Add(imageAssetLogo);

            ImageAssetRequest imageAssetMainImage = new ImageAssetRequest();
            imageAssetMainImage.setAssetId(4);
            imageAssetMainImage.setImageType(ImageAssetTypes.main);
            assets.Add(imageAssetMainImage);

            DataAssetRequest dataAssetDesc = new DataAssetRequest();
            dataAssetDesc.setAssetId(5);
            dataAssetDesc.setDataAssetType(DataAssetTypes.desc);
            dataAssetDesc.setLength(25);
            assets.Add(dataAssetDesc);

            DataAssetRequest dataAssetRating = new DataAssetRequest();
            dataAssetRating.setAssetId(6);
            dataAssetRating.setDataAssetType(DataAssetTypes.rating);
            assets.Add(dataAssetRating);

            return assets;
        }

        private void Button_Load_Ad(object sender, RoutedEventArgs e)
        {
            // Initialize the adview
            MASTNativeAd ad = new MASTNativeAd(this);
            ad.setRequestListener(new AdRequestListener(this));
            ad.setZone(179492); // TODO: Add your ZoneId

            // Request for native assets
            ad.addNativeAssetRequestList(getAssetRequests());

            // Add some custom parameters
            ad.addCustomParameter("keywords", "NFL,Football,Sports,Games,WordsCup");
            ad.addCustomParameter("isp", "T-mobile");
            ad.addCustomParameter("age", "25");
            ad.addCustomParameter("gender", "m");
            ad.addCustomParameter("androidid", "testandroidid1234");
            ad.addCustomParameter("country", "US");
            ad.addCustomParameter("city", "New York, NY");

            ad.setAdNetworkURL(AD_URL);

            // ad.setTest(true);

            // This is by default false.
            //ad.overrideAdapterLoading(false);

            /*
             * Add your device id for Facebook Audience network to get test ads.
             * This will be printed in the logs once you launch the application for
             * the first time.
             */
            //ad.addTestDeviceIdForNetwork(
            //MediationNetwork.FACEBOOK_AUDIENCE_NETWORK, "HASHED_ID");
            // TODO: Add your test device hash id above

            ad.update();
        }

        private class AdRequestListener : MASTNativeAd.NativeRequestListener
        {
            Native context;

            public AdRequestListener(Native context)
            {
                this.context = context;
            }
          
            async void MASTNativeAd.NativeRequestListener.onNativeAdReceived(MASTNativeAd ad)
            {
                if (ad != null)
                {
                    ad.RegisterTrackersForControl(context.NativeAdContainer);

                    List<AssetResponse> nativeAssets = ad.getNativeAssets();

                    for (int i = 0; i < nativeAssets.Count; i++)
                    {
                        AssetResponse asset = nativeAssets.ElementAt(i);

                        if (asset is TitleAssetResponse)
                        {
                            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                            () =>
                            {
                                context.nativeTitle.Text = ((TitleAssetResponse)asset).getTitleText();
                            });
                            continue;
                        }
                        else if (asset is ImageAssetResponse)
                        {
                            switch (((ImageAssetResponse)asset).getImageType())
                            {
                                case ImageAssetTypes.icon:
                                    MASTNativeAd.Image iconImage = ((ImageAssetResponse)asset).getImage();
                                    if (iconImage != null)
                                    {
                                        // Set Icon Image
                                        await ImageDownloader.loadImage(iconImage.getUrl(), context.icon_image);
                                    }
                                    break;

                                case ImageAssetTypes.logo:
                                    // Code to render logo image ...
                                    break;
                                case ImageAssetTypes.main:
                                    MASTNativeAd.Image mainImage = ((ImageAssetResponse)asset).getImage();
                                    if (mainImage != null)
                                    {
                                        // Set Main Image
                                        //await ImageDownloader.loadImage(mainImage.getUrl(),context.main_mage);
                                    }
                                    break;
                            }

                            continue;

                        }
                        else if (asset is DataAssetResponse)
                        {
                            switch (((DataAssetResponse)asset).getDataAssetType())
                            {
                                case DataAssetTypes.desc:
                                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                                    () =>
                                    {
                                        // context.nativeDescription.Text = ((DataAssetResponse)asset).getValue();
                                    });
                                    break;
                                case DataAssetTypes.ctatext:
                                    // Code to render CTA text/button
                                    break;
                                case DataAssetTypes.rating:
                                    // Set Rating
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            void MASTNativeAd.NativeRequestListener.onNativeAdFailed(MASTNativeAd ad, Exception ex)
            {
                // throw new NotImplementedException();
            }

            void MASTNativeAd.NativeRequestListener.onReceivedThirdPartyRequest(MASTNativeAd ad, Dictionary<string, string> properties, Dictionary<string, string> parameters)
            {
                // throw new NotImplementedException();
            }

            void MASTNativeAd.NativeRequestListener.onNativeAdClicked(MASTNativeAd ad)
            {
                // throw new NotImplementedException();
            }
        }
    }
}

