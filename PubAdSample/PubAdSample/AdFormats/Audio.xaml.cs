﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using PubAdSample.Common;
using MASTAdView.video;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace PubAdSample.AdFormats
{
    public sealed partial class Audio : BasicPage, AdManagerListener
    {
        VAdManager adManager = null;
        public Audio()
        {
            this.InitializeComponent();
        }

        private void loadPubMaticAd()
        {
            adManager = new VAdManager(this, onAdRecieved, onAdError, onAdEvent, AD_SERVING_PLATFORM.PUBMATIC);

            Dictionary<string, string> args = new Dictionary<string, string>();
            // Don't allow these to be overridden.
            args.Add("ua", "testapp");
            args.Add("adtype", "13");
            args.Add("pubId", "45710");
            args.Add("siteId", "45711");
            args.Add("adId", "272149");
            args.Add("vadFmt", "2");
            args.Add("vminl", "10");
            args.Add("vmaxl", "60");

            adManager.requestAd("http://demo.tremorvideo.com/proddev/vast/vast_inline_linear.xml?", args);
        }

        public void onAdError(VAdManager adManager, int errorType, int errorCode)
        {
            System.Diagnostics.Debug.WriteLine("Ad request error = " + errorType);
        }

        public void onAdRecieved(VAdManager adManager)
        {
            VAdParams adParams = new VAdParams();

            adParams.linearInfo = new VideoViewInfo();
            adParams.linearInfo.container = this.AudioContainer;
            adParams.linearInfo.player = null;

            adManager.init(adParams);
        }

        public void onAdEvent(VAdManager adManager, AD_EVENT adEvent)
        {
            switch (adEvent)
            {
                case AD_EVENT.AD_LOADED:

                    System.Diagnostics.Debug.WriteLine("onAdEvent = " + adEvent);
                    adManager.PlayAd();
                    break;

            }
        }

        public void thirdPartyRequest()
        {
            throw new NotImplementedException();
        }

        public void contentPauseRequest()
        {
            this.contentPlayer.Pause();
            System.Diagnostics.Debug.WriteLine("Content Paused");
        }

        public void contentResumeRequest()
        {
            this.AudioContainer.Content = this.contentPlayer;
            this.contentPlayer.Play();
            System.Diagnostics.Debug.WriteLine("Content Resumed");

        }

        public bool overrideCompanionHandeling()
        {
            throw new NotImplementedException();
        }

        public void renderCompanionAds()
        {
            throw new NotImplementedException();
        }
        private async void PlayAd_Click(Object sender,
                              RoutedEventArgs e)
        {

            loadPubMaticAd();
        }

    }
}
