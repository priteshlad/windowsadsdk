﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MASTAdView;
using PubAdSample.Common;

namespace PubAdSample.AdFormats
{
    public sealed partial class MRAIDEvents : BasicPage
    {
        private bool isNewInstance = false;
        public MRAIDEvents()
        {
            System.Diagnostics.Debug.WriteLine("Constructor called ");
            isNewInstance = true;
            this.InitializeComponent();
        }

        override
        public void refreshButton_Click(object sender, RoutedEventArgs e)
        {
            adView.Update(true);
            textBlock.Text = "";
        }

        async private void adView_ProcessedRichmediaRequest(object sender, MASTAdView.MASTAdView.ProcessedRichmediaRequestEventArgs e)
        {
            // Since these events can come from a non-main/UI thread, dispatch properly.
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, delegate ()
            {
                textBlock.Text += "URI:" + e.URI + "\n\n";// +" Handled:" + e.Handled + "\n\n";
            });
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            isNewInstance = e.NavigationMode.Equals(NavigationMode.New);
          //  System.Diagnostics.Debug.WriteLine("OnNavigatedTo " + isNewInstance);
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            
        }
       
    }
}
