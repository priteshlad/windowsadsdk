﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PubAdSample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<AdEntry> AdEntryList = new List<AdEntry>();

        public MainPage()
        {
            this.InitializeComponent();
            AdEntryList.Add(new AdEntry("Banner", "Static Image ads", "PubAdSample.AdFormats.Banner"));
            AdEntryList.Add(new AdEntry("InterStitial", "Image ads covering full screen", "PubAdSample.AdFormats.Interstitial"));
            AdEntryList.Add(new AdEntry("RichMedia", "Interactive Image ads supporting open/expand/rotate", "PubAdSample.AdFormats.RichMedia"));
            AdEntryList.Add(new AdEntry("MRAID Events", "Interactive Image ads supporting open/resize/expand/call-to-Action", "PubAdSample.AdFormats.MRAIDEvents"));
            AdEntryList.Add(new AdEntry("Audio", "Simple ad with audio content", "PubAdSample.AdFormats.Audio"));
            AdEntryList.Add(new AdEntry("Video", "Simple ad with Video content", "PubAdSample.AdFormats.Video"));
            AdEntryList.Add(new AdEntry("Native", "Ads rendered by Publisher using ad data provided", "PubAdSample.AdFormats.Native"));
            AdGrid.ItemsSource = AdEntryList;

            /*if (Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.Phone.UI.Input.HardwareButtons"))
            {
                Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            }*/
        }

        private void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
          /*  Debug.WriteLine(" back press app");
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
                e.Handled = true;
            }
            */
        }

        private void Item_Clicked(object sender, ItemClickEventArgs e)
        {
            AdEntry entry = e.ClickedItem as AdEntry;

            Type sampleType = Type.GetType(entry.Tag.ToString(), false);
            Debug.WriteLine("sampletype " + entry.Tag);
            this.Frame.Navigate(sampleType);
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
