﻿using MASTNativeAdView;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace MASTAdView
{
    public class AdDescriptor
    {
        private readonly Dictionary<String, String> adInfo;
        private readonly Dictionary<String, List<string>> adTrackers;

        public AdDescriptor()
        {
            this.adInfo = null;
        }

        public AdDescriptor(Dictionary<String, String> adInfo, Dictionary<String, List<string>> adTrackers)
        {
            this.adInfo = adInfo;
            this.adTrackers = adTrackers;
        }

        public string Type
        {
            get
            {
                string value = null;
                adInfo.TryGetValue("type", out value);
                return value;
            }
        }

        public string URL
        {
            get
            {
                string value = null;
                adInfo.TryGetValue("url", out value);
                return value;
            }
        }

        public string Track
        {
            get
            {
                string value = null;
                adInfo.TryGetValue("track", out value);
                return value;
            }
        }

        public string Image
        {
            get
            {
                string value = null;
                adInfo.TryGetValue("img", out value);
                return value;
            }
        }

        public string Text
        {
            get
            {
                string value = null;
                adInfo.TryGetValue("text", out value);
                return value;
            }
        }

        public string Content
        {
            get
            {
                string value = null;
                adInfo.TryGetValue("content", out value);
                return value;
            }
        }

        #region Trackers

        public List<string> ImpressionTrackers
        {
            get
            {
                List<string> value = null;
                adTrackers.TryGetValue("impressiontrackers", out value);
                return value;
            }
        }

        public List<string> ClickTrackers
        {
            get
            {
                List<string> value = null;
                adTrackers.TryGetValue("clicktrackers", out value);
                return value;
            }
        }

        public List<string> ErrorTrackers
        {
            get
            {
                List<string> value = null;
                adTrackers.TryGetValue("errortrackers", out value);
                return value;
            }
        }
        #endregion
        /*
         * This parameter will be used for Native Ads. It will be set when error
         * occurs while serving the Native ad and server returns a json with error.
         */
        public String errorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                errorMessage = value;
            }
        }

        public override string ToString()
        {
            string ret = string.Format("Type:{0}, URL:{1}", this.Type, this.URL);
            return ret;
        }

        /* @param inputStream Input Stream data to parse
 * @return AdDescriptor containing response for NativeAd
 *
 */
        // @formatter:on
        public static AdDescriptor parseNativeResponse(Stream responseStream)
        {
            NativeAdDescriptor nativeAdDescriptor = null;
            String response;

            StreamReader reader = new StreamReader(responseStream);
            response = reader.ReadToEnd();

            if (!String.IsNullOrEmpty(response))
            {
                List<AssetResponse> nativeAssetList = new List<AssetResponse>();
                String clickUrl = null;
                String fallbackUrl = null;
                String creativeId = null;
                String feedId = null;
                String type = null;
                String subType = null;
                JsonObject mediationObj = null;
                JsonObject nativeObj = null;
                int nativeVersion = 0;
                String mediationPartnerName = null;
                String mediationId = null;
                String adUnitId = null;
                String errorMessage = null;
                String mediationSource = null;
                String[] clickTrackersStringArray = null;
                String[] impressionTrackerStringArray = null;
                String jsTrackerString = null;
                JsonObject ad = null;

                /*String title = null;
                String description = null;
                String callToActionText = null;
                MASTNativeAd.Image mainImage = null;
                MASTNativeAd.Image logoImage = null;
                MASTNativeAd.Image iconImage = null;
                double rating = -1f;
                long downloads = 0;
                String vastTag = null;*/

                JsonArray ads = new JsonArray();
                JsonObject jsonAds = new JsonObject();

                jsonAds = JsonObject.Parse(response);
                ads = jsonAds[MASTNativeAdConstants.RESPONSE_ADS].GetArray();

                if (ads != null && (ad = ads[0].GetObject()) != null
                        && ad.ContainsKey(MASTNativeAdConstants.RESPONSE_ERROR))
                {
                    errorMessage = ad[MASTNativeAdConstants.RESPONSE_ERROR].GetString();

                    if (!String.IsNullOrEmpty(errorMessage))
                    {
                        AdDescriptor adDescriptor = new AdDescriptor();
                        adDescriptor.errorMessage = errorMessage;
                        return adDescriptor;
                    }
                }
                if (ads != null && (ad = ads[0].GetObject()) != null)
                {
                    type = ad[MASTNativeAdConstants.RESPONSE_TYPE].GetString();
                    subType = ad[MASTNativeAdConstants.RESPONSE_SUBTYPE].GetString();

                    double creativeIdInt = ad[MASTNativeAdConstants.RESPONSE_CREATIVEID].GetNumber();
                    creativeId = creativeIdInt.ToString();

                    double feedIdInt = ad[MASTNativeAdConstants.RESPONSE_FEEDID].GetNumber();
                    feedId = feedIdInt.ToString();

                    // Parse third-party response
                    if (ad.ContainsKey(MASTNativeAdConstants.RESPONSE_MEDIATION))
                        mediationObj = ad[MASTNativeAdConstants.RESPONSE_MEDIATION].GetObject();

                    if (mediationObj != null)
                    {

                    }

                    /*
                     * The "link" object is present in case of either
                     * "type=native" OR "type=thirdparty && subtype=native".
                     */
                    if (MASTNativeAdConstants.RESPONSE_NATIVE_STRING.Equals(type)
                            || (MASTNativeAdConstants.RESPONSE_THIRDPARTY_STRING.Equals(type)
                            && MASTNativeAdConstants.RESPONSE_NATIVE_STRING.Equals(subType)))
                    {
                        /* Get the native object */
                        nativeObj = ad[MASTNativeAdConstants.RESPONSE_NATIVE_STRING].GetObject();

                        if (nativeObj != null)
                        {
                            nativeVersion = Convert.ToInt32(nativeObj[MASTNativeAdConstants.RESPONSE_VER].GetNumber());

                            /* Parse impression trackers starts */
                            JsonArray imptracker = nativeObj[MASTNativeAdConstants.RESPONSE_IMPTRACKERS].GetArray();
                            nativeObj.Remove(MASTNativeAdConstants.RESPONSE_IMPTRACKERS);

                            if (imptracker.Count == 0)
                            {
                                impressionTrackerStringArray = new string[0];
                            }

                            for (int i = 0; imptracker != null && i < imptracker.Count; i++)
                            {
                                String url = imptracker[i].GetString();
                                if (impressionTrackerStringArray == null)
                                {
                                    impressionTrackerStringArray = new String[imptracker.Count()];
                                }

                                if (url != null)
                                {
                                    impressionTrackerStringArray[i] = url;
                                }
                            }

                            // Parse jsTracker
                            if (nativeObj.ContainsKey(MASTNativeAdConstants.RESPONSE_JSTRACKER))
                                jsTrackerString = nativeObj[MASTNativeAdConstants.RESPONSE_JSTRACKER].GetString();

                            /* Parse link object and contents */
                            JsonObject linkObj = null;

                            if (nativeObj.ContainsKey(MASTNativeAdConstants.RESPONSE_LINK))
                                linkObj = nativeObj[MASTNativeAdConstants.RESPONSE_LINK].GetObject();

                            if (linkObj != null)
                            {
                                clickUrl = linkObj[MASTNativeAdConstants.RESPONSE_URL].GetString();

                                if (linkObj.ContainsKey(MASTNativeAdConstants.RESPONSE_FALLBACK))
                                    fallbackUrl = linkObj[MASTNativeAdConstants.RESPONSE_FALLBACK].GetString();

                                /* Parse click trackers */
                                JsonArray clktrackerArray = new JsonArray();
                                if (linkObj.ContainsKey(MASTNativeAdConstants.RESPONSE_CLICKTRACKERS))
                                {
                                    clktrackerArray = linkObj[MASTNativeAdConstants.RESPONSE_CLICKTRACKERS].GetArray();
                                    linkObj.Remove(MASTNativeAdConstants.RESPONSE_CLICKTRACKERS);
                                }

                                if (clktrackerArray.Count == 0)
                                {
                                    clickTrackersStringArray = new string[0];
                                }

                                for (int i = 0; clktrackerArray != null && i < clktrackerArray.Count(); i++)
                                {
                                    String clickTrackUrl = clktrackerArray[i].GetString();
                                    if (clickTrackersStringArray == null)
                                    {
                                        clickTrackersStringArray = new String[clktrackerArray.Count()];
                                    }

                                    if (clickTrackUrl != null)
                                    {
                                        clickTrackersStringArray[i] = clickTrackUrl;
                                    }
                                }
                                /* Parse click trackers Ends */
                            }

                            // Parse assets.
                            JsonArray assets = null;
                            if (nativeObj.ContainsKey(MASTNativeAdConstants.NATIVE_ASSETS_STRING))
                                assets = nativeObj[MASTNativeAdConstants.NATIVE_ASSETS_STRING].GetArray();

                            if (assets != null && assets.Count() > 0)
                            {
                                JsonObject asset = null;
                                int assetId = -1;

                                for (int i = 0; i < assets.Count(); i++)
                                {
                                    asset = assets[i].GetObject();
                                    assetId = Convert.ToInt32(asset[MASTNativeAdConstants.ID_STRING].GetNumber());

                                    if (asset.ContainsKey(MASTNativeAdConstants.RESPONSE_IMG))
                                    {
                                        JsonObject imageAssetObj = asset[MASTNativeAdConstants.RESPONSE_IMG].GetObject();
                                        ImageAssetResponse imageAsset = new ImageAssetResponse();
                                        imageAsset.assetId = assetId;
                                        imageAsset.setImage(MASTNativeAd.Image.getImage(imageAssetObj));
                                        if (!String.IsNullOrEmpty(imageAsset.getImage().getUrl()))
                                        {
                                            nativeAssetList.Add(imageAsset);
                                        }
                                        continue;
                                    }
                                    else if (asset.ContainsKey(MASTNativeAdConstants.RESPONSE_TITLE))
                                    {
                                        JsonObject titleAssetObj = asset[MASTNativeAdConstants.RESPONSE_TITLE].GetObject();
                                        TitleAssetResponse titleAsset = new TitleAssetResponse();
                                        titleAsset.assetId = assetId;
                                        titleAsset.setTitleText(titleAssetObj[MASTNativeAdConstants.RESPONSE_TEXT].GetString());
                                        if (!String.IsNullOrEmpty(titleAsset.getTitleText()))
                                        {
                                            nativeAssetList.Add(titleAsset);
                                        }
                                        continue;
                                    }
                                    else if (asset.ContainsKey(MASTNativeAdConstants.RESPONSE_DATA))
                                    {
                                        JsonObject dataAssetObj = asset[MASTNativeAdConstants.RESPONSE_DATA].GetObject();
                                        DataAssetResponse dataAsset = new DataAssetResponse();
                                        dataAsset.assetId = assetId;
                                        dataAsset.value = dataAssetObj[MASTNativeAdConstants.RESPONSE_VALUE].GetString();
                                        if (!String.IsNullOrEmpty(dataAsset.value))
                                        {
                                            nativeAssetList.Add(dataAsset);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                /**
                 * Valid native ad should contain click url, at least one asset
                 * element from the list (main image, icon image, logo image,
                 * title, description), optionally rating, zero or more
                 * impression and click trackers.
                 */
                // @formatter:off
                if ((MASTNativeAdConstants.RESPONSE_NATIVE_STRING.Equals(type))
                    || (MASTNativeAdConstants.RESPONSE_THIRDPARTY_STRING.Equals(type) && MASTNativeAdConstants.RESPONSE_NATIVE_STRING.Equals(subType))
                        && !String.IsNullOrEmpty(clickUrl)
                        && nativeAssetList != null
                        && nativeAssetList.Count() > 0)
                {

                    nativeAdDescriptor = new NativeAdDescriptor(type,
                            nativeVersion, clickUrl, fallbackUrl,
                            impressionTrackerStringArray,
                            clickTrackersStringArray, jsTrackerString,
                            nativeAssetList);

                    nativeAdDescriptor.setNativeAdJSON(response);

                }
                else if (MASTNativeAdConstants.RESPONSE_THIRDPARTY_STRING.Equals(type)
                    && MASTNativeAdConstants.RESPONSE_MEDIATION.Equals(subType)
                        && !String.IsNullOrEmpty(mediationPartnerName)
                        && (!String.IsNullOrEmpty(mediationId) || !String.IsNullOrEmpty(creativeId))
                        && !String.IsNullOrEmpty(adUnitId)
                        && !String.IsNullOrEmpty(mediationSource))
                {
                    nativeAdDescriptor = new NativeAdDescriptor(type,
                            creativeId, mediationPartnerName, mediationId,
                            adUnitId, mediationSource,
                            impressionTrackerStringArray,
                            clickTrackersStringArray, jsTrackerString, feedId);
                    nativeAdDescriptor.setNativeAdJSON(response);
                }
            }

            return nativeAdDescriptor;
        }
    }
}
