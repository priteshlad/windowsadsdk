﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTAdView.video
{

    public delegate void AdRecieved(VAdManager adManager);

    public delegate void AdError(VAdManager adManager, int errorType, int errorCode);

    public delegate void AdEvent(VAdManager adManager, AD_EVENT adEvent);

    public interface AdManagerListener
    {
        void thirdPartyRequest();

        void contentPauseRequest();

        void contentResumeRequest();

        bool overrideCompanionHandeling();

        void renderCompanionAds();

    }
}
