﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Windows.Media;
using Windows.Foundation.Metadata;//UniversalApiContrac;
using Windows.UI.Xaml.Media;
using Windows.Foundation;
using Windows.UI.Xaml.Input;
using System.Net;
using System.IO;
using VastAd;

namespace MASTAdView.video
{
    public enum AD_SERVING_PLATFORM
    {
        MOCEAN,
        PUBMATIC,
        PHOENIX
    }

    public enum AD_EVENT
    {
        AD_LOADED,
        AD_STOPED,
        AD_PAUSED,
        AD_MUTE,
        AD_UNMUTE,
        AD_CLICKED
    }

    public struct VideoViewInfo
    {
        public Frame container;
        public AdPlayerAdapter player;
    }

    public struct NonLinearViewInfo
    {
        public Frame parentView;
    }

    public struct VAdParams
    {

        public VideoViewInfo linearInfo;
        public NonLinearViewInfo nonLiearInfo;
    }

    public interface AdPlayerCallback
    {
        void onAdPlayerPrepared();

        void onAdStarted();

        void onAdPause();

        void onAdResume();

        void onAdMute();

        void onAdUnmute();

        void onAdSkip();

        void onAdStop();

        void onAdComplete();

        void onAdFullScreen();

        void onMinimize();

        void onAdPlayError(int what, int extra);

    }

    public interface AdPlayerAdapter
    {

        void loadMedia(String url);

        void playAd();

        void pauseAd();

        void stopAd();

        void skipAd();

        bool isPlaying();

        void mute();

        void unmute();

        int getTotalDuration();

        int getCurrentPlaybackTime();

        void setAdPlayerCallback(AdPlayerCallback playerCallback);
    }

    enum CreativeType
    {
        Linear,
        NonLinear,
        Companion
    }

    class CompatibleCreative
    {
        public CreativeType creativeType;
        public string mediaUrl;
        public Creative creative;

    }

    public class VAdManager
    {
        private Boolean impressionTracked = false;
        private AD_SERVING_PLATFORM adPlatform;
        private AdManagerListener adManagerListener;
        private AdRecieved adRecievedDelegate;
        private AdError adErrorDelegate;
        private AdEvent adEventDelegate;
        private MediaElement videoPlayer;
        private MediaElement contentPlayerReference;
        private SystemMediaTransportControls systemControls;
        private Vast vast;
        private Dictionary<string, string> requestParameters;
        private CompatibleCreative compatibleCreative;

        public object MediaState { get; private set; }
        private TimeSpan playerPosition;
        public bool isRecreatedInstance { get; set; }
        

        public VAdManager()
        {

        }

        public VAdManager(AdManagerListener adManagerListener, AdRecieved adRecieved, AdError adError, AdEvent adEvent, AD_SERVING_PLATFORM adPlatform)
        {
            if (adManagerListener == null)
                throw (new ArgumentException());

            this.adManagerListener = adManagerListener;
            this.adRecievedDelegate = adRecieved;
            this.adErrorDelegate = adError;
            this.adEventDelegate = adEvent;
            this.adPlatform = adPlatform;
        }

        public void requestAd(string requestUrl, Dictionary<string, string> args)
        {

            //this.adDelegate.adRecieved(this);
            this.requestParameters = args;
            VastRequestHandler vastRequestHandler = new VastRequestHandler(vastResponseRecieved, vastRequestFailed);
            vastRequestHandler.requestAd(requestUrl, args);
        }

        private void vastResponseRecieved(Vast vast)
        {
            this.vast = vast;
            this.adRecievedDelegate(this);
        }

        private void vastRequestFailed(int errorType, int errorCode)
        {
            this.adErrorDelegate(this, errorType, errorCode);
        }

        public async void init(VAdParams adParams)
        {
            System.Diagnostics.Debug.WriteLine("init : adParams = " + adParams.linearInfo);

            //var task = new Task(() => load_async_media(adParams));
            //task.Start();

            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                () =>
                { load_async_media(adParams); });

        }

        private void load_async_media(VAdParams adParams)
        {
            Debug.WriteLine("LOAD ASYNC MEDIA");

            //this.requestParameters.ContainsKey("vtype") == true && 
            if (this.vast != null && this.vast.ads.Count() > 0)
            {
                //Handle linear ad tag
                //     if (this.requestParameters["vtype"].Contains("linear") == true)
                //TODO: made true for testing only linear ads, replace it later
                if (true)
                {
                    CompatibleCreative linearCreative = getCompatibleCreative(CreativeType.Linear);
                    compatibleCreative = linearCreative;
                    if (linearCreative != null && linearCreative.mediaUrl != null)
                    {

                        //Create player & Load media file
                        videoPlayer = new MediaElement();
                        videoPlayer.AutoPlay = false;
                        // videoPlayer.Source = new Uri("http://rmcdn.2mdn.net/MotifFiles/html/1248596/android_1330378998288.mp4", UriKind.Absolute);
                        videoPlayer.Source = new Uri(linearCreative.mediaUrl, UriKind.Absolute);

                        videoPlayer.AreTransportControlsEnabled = true;
                        videoPlayer.MediaFailed += player_MediaFailed;
                        videoPlayer.MediaEnded += player_MediaEnded;
                        videoPlayer.MediaOpened += player_MediaOpened;
                        videoPlayer.CurrentStateChanged += MusicPlayer_CurrentStateChanged;
                        videoPlayer.TransportControls.Tapped += player_Clicked;
                        // videoPlayer.TransportControls.IsTapEnabled = false;
                        // videoPlayer.TransportControls.IsHitTestVisible = false;

                        // Hook up app to system transport controls.
                        // Register to handle the following system transpot control buttons.
                        systemControls = SystemMediaTransportControls.GetForCurrentView();
                        systemControls.ButtonPressed += SystemControls_ButtonPressed;
                        systemControls.IsPlayEnabled = true;
                        systemControls.IsPauseEnabled = true;

                        this.contentPlayerReference = (MediaElement)adParams.linearInfo.container.Content;
                        adParams.linearInfo.container.Content = videoPlayer;

                        Debug.WriteLine("Init : duration" + videoPlayer.NaturalDuration);
                        this.adEventDelegate(this, AD_EVENT.AD_LOADED);
                    }
                    else
                    {
                        Debug.WriteLine("Compatible Linear creative not fund.");
                        this.adErrorDelegate(this, -1, -1);
                    }

                }
                else if (this.requestParameters["vtype"].Contains("non-linear") == true)
                {
                    this.adErrorDelegate(this, -1, -1);

                    Debug.WriteLine("Publisher provided Non-Linear vtype but it is not supported at present.");
                }
                else
                {
                    Debug.WriteLine("Publisher does not provided linear/non-linear vtype.");
                }
            }
            else
            {

            }

        }

        private CompatibleCreative getCompatibleCreative(CreativeType type)
        {

            foreach (Ad ad in this.vast.ads)
            {
                if (ad.adType == VastAdType.VastAdTypeInLine && ad.creatives.Count() > 0)
                {
                    foreach (Creative creative in ad.creatives)
                    {
                        if (type == CreativeType.Linear && creative is Linear)
                        {
                            foreach (MediaFile mediaFile in ((Linear)creative).mediaFiles)
                            {
                                if (mediaFile != null && mediaFile.mediaUrl != null)
                                {
                                    if (mediaFile.mediaUrl.Contains(".mp4") ||
                                       mediaFile.mediaUrl.Contains(".flv") ||
                                       mediaFile.mediaUrl.Contains(".mov") ||
                                       mediaFile.mediaUrl.Contains(".3gpp") ||
                                       mediaFile.mediaUrl.Contains(".wmv") ||
                                       mediaFile.mediaUrl.Contains(".avi") ||
                                       //For audio
                                       mediaFile.mediaUrl.Contains(".mp3") ||
                                       mediaFile.mediaUrl.Contains(".3gp") ||
                                       mediaFile.mediaUrl.Contains(".amr") ||
                                       mediaFile.mediaUrl.Contains(".ac3") ||
                                       mediaFile.mediaUrl.Contains(".ec3") ||
                                       mediaFile.mediaUrl.Contains(".wav"))
                                    {
                                        //TODO::AT present, returning 1st matched creative but lateron need 
                                        //to return actual compatible creative out of all matched creatives.
                                        CompatibleCreative compatibleCreative = new CompatibleCreative();
                                        compatibleCreative.creativeType = CreativeType.Linear;
                                        compatibleCreative.mediaUrl = mediaFile.mediaUrl;
                                        compatibleCreative.creative = creative;
                                        return compatibleCreative;
                                    }
                                }
                            }
                        }
                        else if (type == CreativeType.NonLinear && creative is NonLinear)
                        {

                        }
                    }
                }
            }
            return null;
        }
        //------------------ Start of Media Player ----------------------

        void SystemControls_ButtonPressed(SystemMediaTransportControls sender,
                                            SystemMediaTransportControlsButtonPressedEventArgs args)
        {

            Debug.WriteLine("SystemMediaTransportControlsButton ======");
            switch (args.Button)
            {
                case SystemMediaTransportControlsButton.Play:
                    Debug.WriteLine("SystemMediaTransportControlsButton.Play");
                    //videoPlayer.Play();
                    PlayMedia();
                    break;
                case SystemMediaTransportControlsButton.Pause:
                    Debug.WriteLine("SystemMediaTransportControlsButton.Pause");
                    //videoPlayer.Pause();
                    PauseMedia();
                    break;
                default:
                    break;
            }
        }

        async void PlayMedia()
        {
            Debug.WriteLine("PLAYMEDIA");
           await Window.Current.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                videoPlayer.Play();
            });
        }

        async void PauseMedia()
        {
            await Window.Current.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                videoPlayer.Pause();
            });
        }


        void MusicPlayer_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("MEDIA PLAYER STATE CHANGED ");
            switch (videoPlayer.CurrentState)
            {
                case MediaElementState.Playing:
                    Debug.WriteLine("PLAYING");
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Playing;

                    if (!isRecreatedInstance)
                    {
                        isRecreatedInstance = true;
                        trackImpressions();
                        trackEvent("creativeView");
                        trackEvent("start");
              
                    }

                    break;
                case MediaElementState.Paused:
                    Debug.WriteLine("PAUSED");
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Paused;
                    break;
                case MediaElementState.Stopped:
                    Debug.WriteLine("STOPED");
                    StopQuartileTimer();
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Stopped;

                    this.adManagerListener.contentResumeRequest();

                    break;
                case MediaElementState.Closed:
                    Debug.WriteLine("CLOSED");
                    systemControls.PlaybackStatus = MediaPlaybackStatus.Closed;
                    break;
                case MediaElementState.Buffering:
                    Debug.WriteLine("BUFFERING");
                    break;
                case MediaElementState.Opening:
                    Debug.WriteLine("OPENING");
                    break;

                default:
                    break;
            }
        }
        //-----

        private void trackEvent(string name)
        {
            Linear creative = (Linear)this.compatibleCreative.creative;


            this.impressionTracked = true;
            List<Tracking> eventTrackers = creative.trackingEvents;
            foreach (Tracking tracker in eventTrackers)
            {
                //TODO : hit url
                if (tracker.eventName.CompareTo(name) == 0)
                {
                    Debug.WriteLine("------ Tracking  -" + tracker.URI + "For event :" + name);

                    //hit tracker.URI.
                    NetworkHandler.StartWebRequest(tracker.URI, (result, exception) => {
                        if (result != null)
                        {
                            Debug.WriteLine("------ Tracked  -" + tracker.URI + "Result " + result.StatusDescription + result.StatusCode);
                        }

                        if (exception != null)
                        {
                            Debug.WriteLine("------ Error -" + exception + " for URL  -" + tracker.URI);
                        }


                    });

                }
            }
        }

        /*
        private void StartWebRequest(string url, Action<HttpWebResponse,Exception> FinishWebRequest)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.BeginGetResponse(result =>
            {

                try
                {
                    System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.EndGetResponse(result);
                    FinishWebRequest(response, null);

                }
                catch (Exception exception)
                {
                    FinishWebRequest(null, exception);

                }

            }, null); // Don't need the state here any more
        }
        */

        private void trackClikEvent()
        {

            Linear creative = (Linear)this.compatibleCreative.creative;
            List<String> clickTrackers = creative.videoClickTrackingURIs;
            foreach (string url in clickTrackers)
            {
                //TODO : hit url
                Debug.WriteLine("------ Tracking  -" + url);

                NetworkHandler.StartWebRequest(url, (result,exception) => {

                    if (result != null)
                    {
                        Debug.WriteLine("------ Tracked  -" + url + "Result " + result.StatusDescription + result.StatusCode);
                    }

                    if (exception != null)
                    {
                        Debug.WriteLine("------ Error -" + exception + " for URL  -" + url);
                    }


                });


            }
        }

        private void trackImpressions()
        {

            if (this.impressionTracked)
            {
                return;
            }
            this.impressionTracked = true;
            List<String> impressionTrackers = this.vast.currentAd().impressionTrackers;
            foreach (string url in impressionTrackers)
            {
                Debug.WriteLine("------ Tracking  -" + url);

                //TODO : hit url
                NetworkHandler.StartWebRequest(url, (result, exception) => {

                    if (result != null)
                    {
                        Debug.WriteLine("------ Tracked  -" + url + "Result " + result.StatusDescription + result.StatusCode);
                    }

                    if (exception != null)
                    {
                        Debug.WriteLine("------ Error -" + exception + " for URL  -" + url);
                    }


                });

            }
        }

        private void player_Clicked(Object sender, TappedRoutedEventArgs e)
        {
            MediaTransportControls c = (MediaTransportControls)sender;

            Point point = e.GetPosition(c);
            //Constant height of media player control - 120
            if (point.Y < (c.ActualHeight - 120))
            {
                playerPosition = getPosition();
                Debug.WriteLine("player_Clicked playerPosition "+ playerPosition);

                //Fire Click trackers
                trackClikEvent();
               
                // Go to landing page
                Linear creative = (Linear)this.compatibleCreative.creative;
                Frame frame = Window.Current.Content as Frame;
                frame.Navigate(typeof(InternalBrowserPage), creative.videoClickThroughURI);
            }

        }

        private void player_MediaEnded(object sender, RoutedEventArgs e)
        {

            if (!FOURTH_QUARTILE_SENT)
            {
                Debug.WriteLine("FOURTH_QUARTILE_REACHED");
                FOURTH_QUARTILE_SENT = true;
                trackEvent("complete");

                StopQuartileTimer();
            }
            videoPlayer.Stop();
            Debug.WriteLine("player_MediaEnded");
        }

        private void player_MediaOpened(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("player_MediaOpened");
            totalMediaDuartion = videoPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
            Debug.WriteLine("player_MediaOpened : duration" + videoPlayer.NaturalDuration + ",  totalMediaDuartion = " + totalMediaDuartion);
            if (isRecreatedInstance)
                videoPlayer.Position = playerPosition;
            Debug.WriteLine("VIDEO POSITION "+ videoPlayer.Position);
            SetupQuartileTimer();
        }

        private void player_MediaFailed(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("player_MediaFailed");
            this.adManagerListener.contentResumeRequest();
        }

        public void PlayAd()
        {
            this.adManagerListener.contentPauseRequest();
            videoPlayer.AutoPlay = true;
            videoPlayer.Play();
            Debug.WriteLine("Play() called.");
        }

        public String getDuration()
        {
            //videoPlayer.IsFullWindow = !videoPlayer.IsFullWindow;
            return videoPlayer.NaturalDuration.ToString();
        }


        public TimeSpan getPosition()
        {
            return videoPlayer.Position;
        }
        //-------------------------- End --------------------------------

        #region DispatcherTimer for quartile event

        private DispatcherTimer _quartileTimer;

        bool FIRST_QUARTILE_SENT = false;
        bool SECOND_QUARTILE_SENT = false;
        bool THIRD_QUARTILE_SENT = false;
        bool FOURTH_QUARTILE_SENT = false;

        double totalMediaDuartion = 0;


        private void SetupQuartileTimer()
        {
            _quartileTimer = new DispatcherTimer();
            _quartileTimer.Interval = TimeSpan.FromMilliseconds(200);
            StartQuartileTimer();
        }

        private void _quartile_timer_Tick(object sender, object e)
        {
            double currentTime = videoPlayer.Position.TotalMilliseconds;
            if (currentTime >= (0.75 * totalMediaDuartion) && !THIRD_QUARTILE_SENT)
            {
                Debug.WriteLine("THIRD_QUARTILE_REACHED");
                trackEvent("thirdQuartile");

                THIRD_QUARTILE_SENT = true;
            }
            else if (currentTime >= (totalMediaDuartion / 2) && !SECOND_QUARTILE_SENT)
            {
                Debug.WriteLine("SECOND_QUARTILE_SENT");
                trackEvent("midpoint");

                SECOND_QUARTILE_SENT = true;
            }
            else if (currentTime >= (totalMediaDuartion / 4) && !FIRST_QUARTILE_SENT)
            {
                Debug.WriteLine("FIRST_QUARTILE_SENT");
                trackEvent("firstQuartile");

                FIRST_QUARTILE_SENT = true;
            }
            else
            {

            }
        }

        private void StartQuartileTimer()
        {
            _quartileTimer.Tick += _quartile_timer_Tick;
            _quartileTimer.Start();
        }

        private void StopQuartileTimer()
        {
            _quartileTimer.Stop();
            _quartileTimer.Tick -= _quartile_timer_Tick;
        }
        #endregion

    }
}
