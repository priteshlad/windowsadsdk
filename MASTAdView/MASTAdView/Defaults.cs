﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTAdView
{

    public class Defaults
    {
        public static string VERSION = "3.1.2";

        public static string AD_SERVER_URL = "http://ads.moceanads.com/ad";

        // This is the user agent string sent if unable to determine one from the browser control.

        public static string ERROR_USER_AGENT = "MASTAdView/" + VERSION + " (Windows , unable to determine ua)";

        public static int NETWORK_TIMEOUT_MILLISECONDS = 5000;

        // How much content is allowed after parsing out click url and image or text content before
        // falling through and rendering as html vs. native rendering.
        public static int DESCRIPTOR_THIRD_PARTY_VALIDATOR_LENGTH = 20;


        public static string RICHMEDIA_SCRIPT_RESOURCE = "MASTAdView.Resources.MASTMRAIDController.js";
        public static string RICHMEDIA_HTML_RESOURCE = "MASTAdView.Resources.mraidhtml.txt";
        public static string CLOSE_BUTTON_RESOURCE = "MASTAdView.Resources.MASTCloseButton.png";

        // Internal browser toolbar images:
        public static string IB_CLOSE_BUTTON_RESOURCE = "MASTAdView.Resources.cancel.png";
        public static string IB_BACK_BUTTON_RESOURCE = "MASTAdView.Resources.back.png";
        public static string IB_FORWARD_BUTTON_RESOURCE = "MASTAdView.Resources.next.png";
        public static string IB_REFRESH_BUTTON_RESOURCE = "MASTAdView.Resources.refresh.png";
        public static string IB_OPEN_BUTTON_RESOURCE = "MASTAdView.Resources.upload.png";

        public static int CLOSE_BUTTON_SIZE = 50;
        public static int INLINE_CLOSE_BUTTON_SIZE = 28;

        public static string RICHMEDIA_FORMAT = "<!DOCTYPE html><html><head><meta name=\"viewport\" content=\"width=device-width, user-scalable=0;\"/><script>{0}</script><style>body{{margin:0;padding:0;}}</style></head><body>{1}</body></html>";

        // The default movement threshold distance in meters.
        public static double LOCATION_DETECTION_MOVEMENT_THRESHOLD = 1000;
    }
}
