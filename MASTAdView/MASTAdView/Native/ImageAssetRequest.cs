﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class ImageAssetRequest : AssetRequest
    {
        /** Image Type */
        private ImageAssetTypes imageType;

        /** Image width */
        public int width;

        /** Image height */
        public int height;

        public ImageAssetTypes getImageType()
        {
            return imageType;
        }

        public void setImageType(ImageAssetTypes imageType)
        {
            this.imageType = imageType;
        }

        public int getWidth()
        {
            return width;
        }

        public void setWidth(int width)
        {
            this.width = width;
        }

        public int getHeight()
        {
            return height;
        }

        public void setHeight(int height)
        {
            this.height = height;
        }

        public int getTypeId()
        {
            if (imageType == ImageAssetTypes.icon)
                return 1;
            else if (imageType == ImageAssetTypes.logo)
                return 2;
            else if (imageType == ImageAssetTypes.main)
                return 3;
            else
                return -1;
        }
    }
}
