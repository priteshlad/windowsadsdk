﻿using MASTAdView;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace MASTNativeAdView
{
    public class MASTNativeAd
    {
        public enum LogLevel
        {
            None, Error, Debug
        }

        private enum CallbackType
        {
            NativeReceived, NativeFailed, ThirdPartyReceived, NativeAdClicked
        }

        /**
         * Invoked when the SDK logs events. If applications override logging
         * they can return true to indicate the log event has been consumed and
         * the SDK processing is not needed.
         * <p>
         * Will not be invoked if the MASTNativeAd instance's logLevel is set
         * lower than the event.
         * 
         * @param nativeAd
         * @param event
         *            String representing the event to be logged.
         * @param eventLevel
         *            LogLevel of the event.
         * @return
         */
        public interface LogListener
        {
            bool onLogEvent(MASTNativeAd nativeAd, string event1, LogLevel eventLevel);
        }

        public interface NativeRequestListener
        {
            /**
             * Callback when the native ad is received.
             * 
             * @param ad
             */
            void onNativeAdReceived(MASTNativeAd ad);

            /**
             * Callback when MASTNativeAd fails to get an ad. This may be due to
             * invalid zone, network connection not available, timeout, no Ad to
             * fill at this point.
             * 
             * @param ad
             *            - MASTNativeAd object which has requested for the Ad.
             * @param ex
             *            - Exception occurred.
             */
            void onNativeAdFailed(MASTNativeAd ad, Exception ex);

            /**
             * Third party ad received. The application should be expecting this and
             * ready to render the ad with the supplied configuration.
             * 
             * @param ad
             * @param properties
             *            Properties of the ad request (ad network information).
             * @param parameters
             *            Parameters for the third party network (expected to be
             *            passed to that network).
             */
            void onReceivedThirdPartyRequest(MASTNativeAd ad,
                   Dictionary<String, String> properties, Dictionary<String, String> parameters);

            /**
             * Callback when the user clicks on the native ad. Implement your logic
             * on view click in this method.
             * 
             * <B> Do not implement your own clickListener in your activity. </b>
             * 
             * @param ad
             */
            void onNativeAdClicked(MASTNativeAd ad);
        }

        private int mZone = 0;
        //private Dictionary<MediationNetwork, String> mMapMediationNetworkTestDeviceIds = null;
        // Used to store the the current native response
        private Page mContext;
        private NativeAdDescriptor mNativeAdDescriptor = null;
        private bool mImpressionTrackerSent = false;
        private bool mThirdPartyImpTrackerSent = false;
        private bool mThirdPartyClickTrackerSent = false;
        private bool mClickTrackerSent = false;
        private NativeRequestListener mListener = null;
        private String mUserAgent = null;
        private LogListener mLogListener = null;
        private LogLevel mLogLevel = LogLevel.None;
        private bool test = false;
        private Dictionary<String, String> mAdRequestParameters;
        private List<AssetRequest> mRequestedNativeAssets;
        //private MASTBaseAdapter mBaseAdapter = null;
        //private BrowserDialog mBrowserDialog = null;
        // Use external system native browser by default
        private bool mUseInternalBrowser = false;
        //private MediationData mMediationData = null;

        /**
        * If false, denotes that SDK should handle the third party request with the
        * help of adapter. If true, SDK should give the control back to publisher
        * to handle third party request.
        */
        private bool mOverrideAdapter = true;
        private String mNetworkUrl = Defaults.AD_NETWORK_URL;

        // Location support
        //private LocationManager locationManager = null;
        //private LocationListener locationListener = null;

        private AdRequest mAdRequest = null;

        /**
         * @param context
         */
        public MASTNativeAd(Page context)
        {
            mContext = context;
            mAdRequestParameters = new Dictionary<String, String>();
            mRequestedNativeAssets = new List<AssetRequest>();
        }

        /**
         * Sets the zone on the ad network to obtain ad content.
         * <p>
         * REQUIRED - If not set updates will fail.
         * 
         * @param zone
         *            Ad network zone.
         */
        public void setZone(int zone)
        {
            this.mZone = zone;
        }

        /**
         * Returns the currently configured ad network zone.
         * 
         * @return Ad network zone.
         */
        public int getZone()
        {
            return mZone;
        }

        /**
         * Sets the instance test mode. If set to test mode the instance will
         * request test ads for the configured zone.
         * <p>
         * Warning: This should never be enabled for application releases.
         * 
         * @param test
         *            true to set test mode, false to disable test mode.
         */
        public void setTest(bool test)
        {
            this.test = test;
        }

        /**
         * Access for test mode state of the instance.
         * 
         * @return true if the instance is set to test mode, false if test mode is
         *         disabled.
         */
        public bool isTest()
        {
            return test;
        }

        public void setRequestListener(NativeRequestListener requestListener)
        {
            if (requestListener == null
                    || !(requestListener is NativeRequestListener))
            {
                /*throw new IllegalArgumentException(
                        "Kindly pass the object of type NativeRequestListener in case you want to use NativeAdView."
                                + " Implement NativeRequestListener in your activity instead of RequestListener. ");*/
            }
            else
            {
                mListener = requestListener;
            }
        }

        /**
         * Sets the log listener. This listener provides the ability to override
         * default logging behavior.
         * 
         * @param logListener
         *            MASTNativeAd.LogListener implementation
         */
        public void setLogListener(LogListener logListener)
        {
            this.mLogListener = logListener;
        }

        /**
         * Returns the currently configured listener.
         * 
         * @return MASTNativeAd.LogListener set with setLogListener().
         */
        public LogListener getLogListener()
        {
            return mLogListener;
        }

        /**
         * Sets the log level of the instance. Logging is done through console
         * logging.
         * 
         * @param logLevel
         *            LogLevel
         */
        public void setLogLevel(LogLevel logLevel)
        {
            this.mLogLevel = logLevel;
        }

        /**
         * Returns the currently configured log level.
         * 
         * @return currently configured LogLevel
         */
        public LogLevel getLogLevel()
        {
            return mLogLevel;
        }

        /**
         * List of requested native assets.
         * <p>
         * Returns subclasses of {@link AssetRequest} class viz:
         * {@link TitleAssetRequest}, {@link ImageAssetRequest} and
         * {@link DataAssetRequest}.
         * <p>
         * Sub-type of asset can be identified by 'type' variable where ever
         * available.
         * 
         * @return
         */
        public List<AssetRequest> getRequestedNativeAssetsList()
        {
            return mRequestedNativeAssets;
        }

        /**
         * Add native asset request in ad request.
         * <p>
         * <b>NOTE:</b> Use unique assetId for each asset in asset request. The same
         * assetId will be returned in native ad response. You can use this assetId
         * to map requested native assets with response.
         * 
         * @see addNativeAssetRequestList(List<AssetRequest> assetList) :
         *      Convenience method to add all assets at once.
         * @param asset
         *            Native {@link AssetRequest} to add in the ad request
         */
        public void addNativeAssetRequest(AssetRequest asset)
        {
            if (mRequestedNativeAssets != null && asset != null)
            {
                mRequestedNativeAssets.Add(asset);
            }
        }

        /**
        * Convenience method to add all native asset requests at once. *
        * <p>
        * <b>NOTE:</b> Use unique assetId for each asset in asset request. The same
        * assetId will be returned in native ad response. You can use this assetId
        * to map requested native assets with response.
        * 
        * @see addNativeAssetRequest(AssetRequest asset)
        * @param assetList
        *            List of Native {@link AssetRequest} to add in the ad request
        */
        public void addNativeAssetRequestList(List<AssetRequest> assetList)
        {
            if (mRequestedNativeAssets != null && assetList != null)
            {
                // Put all the user sent parameters in the request
                foreach (var asset in assetList)
                    mRequestedNativeAssets.Add(asset);
            }
        }

        /**
         * Collection of ad request parameters. Allows setting extra network
         * parameters.
         * <p>
         * The SDK will set various parameters based on configuration and other
         * options. For more information see
         * http://developer.moceanmobile.com/Mocean_Ad_Request_API.
         * 
         * @return Map containing optional request parameters.
         */
        public Dictionary<String, String> getAdRequestParameters()
        {
            return mAdRequestParameters;
        }

        /**
         * Allows setting of extra custom parameters to ad request. Add custom
         * parameter (key-value).
         * 
         * @see - For details of custom parameters refer
         *      http://developer.moceanmobile.com/Mocean_Ad_Request_API
         */
        public void addCustomParameter(String customParamName, String value)
        {
            if (mAdRequestParameters != null && customParamName != null)
            {
                mAdRequestParameters.Add(customParamName, value);
            }
        }

        /**
         * Convenience method to add all custom parameters in one Map. Allows
         * setting of extra custom parameters to ad request.
         * 
         * @param customParamMap
         *            Map containing custom parameters
         * @see - For details of custom parameters refer
         *      http://developer.moceanmobile.com/Mocean_Ad_Request_API
         */
        public void addCustomParametersMap(Dictionary<String, String> customParamMap)
        {
            if (mAdRequestParameters != null && customParamMap != null)
            {
                foreach (var asset in customParamMap)
                    mAdRequestParameters.Add(asset.Key, asset.Value);
            }
        }

        /**
         * Specifies the URL of the ad network. This defaults to Mocean's ad
         * network.
         * 
         * @param adNetworkURL
         *            URL of the ad server (ex: http://ads.moceanads.com/ad);
         */
        public void setAdNetworkURL(String adNetworkURL)
        {
            mNetworkUrl = adNetworkURL;
        }

        /**
         * Returns the currently configured ad network.
         * 
         * @return Currently configured ad network URL.
         */
        public String getAdNetworkURL()
        {
            return mNetworkUrl;
        }

        /**
         * Updates ad.
         */
        public void update()
        {
            internalUpdate(false);
        }

        /**
         * This is an internal method used to update and make a new request to the
         * server. If defaulted is true then the id of the defaulted ad network is
         * added to the request so that this network will be removed from the
         * auction at PubMatic side.
         * 
         * @param defaulted
         *            denotes whether third party ad network defaulted
         */
        private void internalUpdate(bool defaulted)
        {
            Dictionary<String, String> args = new Dictionary<String, String>();

            reset();

            initUserAgent();

            /*try
            {
                TelephonyManager tm = (TelephonyManager)mContext
                        .getSystemService(Context.TELEPHONY_SERVICE);
                String networkOperator = tm.getNetworkOperator();
                if ((networkOperator != null) && (networkOperator.length() > 3))
                {
                    String mcc = networkOperator.substring(0, 3);
                    String mnc = networkOperator.substring(3);

                    args.put("mcc", String.valueOf(mcc));
                    args.put("mnc", String.valueOf(mnc));
                }
            }
            catch (Exception ex)
            {
                logEvent("Unable to obtain mcc and mnc. Exception:" + ex,
                        LogLevel.Debug);
            }*/

            // Put all the user sent parameters in the request
            foreach (var adRequestParameters in mAdRequestParameters)
                args.Add(adRequestParameters.Key, adRequestParameters.Value);

            // Don't allow these to be overridden.
            args.Add(MASTNativeAdConstants.REQUESTPARAM_UA, mUserAgent);
            args.Add(MASTNativeAdConstants.REQUESTPARAM_SDK_VERSION, Defaults.SDK_VERSION);
            args.Add(MASTNativeAdConstants.REQUESTPARAM_COUNT, Defaults.NATIVE_REQUEST_COUNT);
            // Response type for Generic Json
            args.Add(MASTNativeAdConstants.REQUESTPARAM_KEY, Defaults.NATIVE_REQUEST_KEY);
            // Ad type for native.
            args.Add(MASTNativeAdConstants.REQUESTPARAM_TYPE, Defaults.NATIVE_REQUEST_AD_TYPE);
            args.Add(MASTNativeAdConstants.REQUESTPARAM_ZONE, mZone.ToString());

            //args.Add(MASTNativeAdConstants.REQUESTPARAM_TEST, Defaults.NATIVE_REQUEST_TEST_TRUE);

            try
            {
                if (mAdRequest != null)
                    mAdRequest.Cancel();

                mAdRequest = AdRequest.InitiateRequest(Defaults.NETWORK_TIMEOUT_SECONDS, mNetworkUrl, mUserAgent, args, generateNativeAssetRequestJson(),
                                new AdRequestServed(adRequestServed),
                                new AdRequestError(adRequestError),
                                new AdRequestFailed(adRequestFailed),
                                true);

                //String requestUrl = mAdRequest.getRequestUrl();

                //String requestUrl = mAdRequest.getRequestUrl();
                //logEvent("Ad request:" + requestUrl, LogLevel.Debug);
            }
            catch (Exception e)
            {
                //logEvent("Exception encountered while generating ad request URL:"
                //+ e, LogLevel.Error);

                fireCallback(CallbackType.NativeFailed, e);
            }
        }

       async public void RegisterTrackersForControl(UIElement control)
        {
            // Register the control for click trackers.

            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                control.Tapped += FireClickTracker;
            });

            // Fire ImpressionTrackers here.
            try
            {
                foreach (string trackerUrl in mNativeAdDescriptor.getNativeAdImpressionTrackers())
                {
                    if (trackerUrl != null && trackerUrl.Length != 0)
                    {
                        NetworkHandler.StartWebRequest(trackerUrl, (result, exception) => {
                            if (result != null)
                            {
                                Debug.WriteLine("------ Tracked  -" + trackerUrl + "Result " + result.StatusDescription + result.StatusCode);
                            }

                            if (exception != null)
                            {
                                Debug.WriteLine("------ Error -" + exception + " for URL  -" + trackerUrl);
                            }
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            
        }

        private void FireClickTracker(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                foreach (string trackerUrl in mNativeAdDescriptor.getNativeAdClickTrackers())
                {
                    if (trackerUrl != null && trackerUrl.Length != 0)
                    {
                        NetworkHandler.StartWebRequest(trackerUrl, (result, exception) => {
                            if (result != null)
                            {
                                Debug.WriteLine("------ Tracked  -" + trackerUrl + "Result " + result.StatusDescription + result.StatusCode);
                            }

                            if (exception != null)
                            {
                                Debug.WriteLine("------ Error -" + exception + " for URL  -" + trackerUrl);
                            }
                        });
                    }
                }

                ShowInternalBrowser(mNativeAdDescriptor.getClick());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void ShowInternalBrowser(string url)
        {

            Frame frame;
            Page page = mContext;
            frame = (page == null ? Window.Current.Content : page.Frame) as Frame;
            frame.Navigate(typeof(InternalBrowserPage), url);

            //OnInternalBrowserOpened();
        }


        public void reset()
        {

            if (mAdRequest != null)
            {
                mAdRequest.Cancel();
            }

            mNativeAdDescriptor = null;
            //mMediationData = null;

            mImpressionTrackerSent = false;
            mThirdPartyImpTrackerSent = false;
            mThirdPartyClickTrackerSent = false;
            mClickTrackerSent = false;

            /*if (mBaseAdapter != null)
            {
                mBaseAdapter.destroy();
                mBaseAdapter = null;
            }*/
        }

        public void destroy()
        {
            reset();

            mOverrideAdapter = false;
            mListener = null;
        }

        /**
         * Get the list of native assets.
         * 
         * @return List of {@link AssetResponse} if response contains any assets
         *         else returns null
         */
        public List<AssetResponse> getNativeAssets()
        {
            if (mNativeAdDescriptor != null
                    && mNativeAdDescriptor.getNativeAssetList() != null
                    && mNativeAdDescriptor.getNativeAssetList().Count > 0)
            {
                return mNativeAdDescriptor.getNativeAssetList();
            }
            return null;
        }

        /**
         * Get the javascript tracker received in native response. <br>
         * The javascript tracker is represented by 'jstracker' object in OpenRTB
         * Native ad specification.
         * <p>
         * <b>Note:</b> If jstracker is present, publisher should execute this
         * javascript at impression time (after onNativeAdReceived callback)
         * whenever possible.
         * 
         * @return JsTracker as string if present, else returns null.
         */
        public String getJsTracker()
        {
            if (mNativeAdDescriptor != null
                    && mNativeAdDescriptor.getJsTracker() != null)
            {
                return mNativeAdDescriptor.getJsTracker();
            }
            return null;
        }

        /**
         * Get the landing url whenever user clicks on the Native Ad
         * 
         * @return landing page url
         */
        public String getClick()
        {
            if (mNativeAdDescriptor != null)
            {
                return mNativeAdDescriptor.getClick();
            }

            return null;
        }

        /**
         * Returns the native ad response in json format.
         * 
         * @return
         */
        public String getAdResponse()
        {
            if (mNativeAdDescriptor != null)
            {
                return mNativeAdDescriptor.getNativeAdJSON();
            }

            return null;
        }

        // Private methods start
        private void initUserAgent()
        {
            if (String.IsNullOrEmpty(mUserAgent))
            {
                /*WebView webView = new WebView(mContext);
                mUserAgent = webView.getSettings().getUserAgentString();

                if (TextUtils.isEmpty(mUserAgent))
                {
                    mUserAgent = Defaults.USER_AGENT;
                }

                webView = null;*/

                mUserAgent = Defaults.USER_AGENT;
            }
        }

        private void logEvent(String event1, LogLevel eventLevel)
        {
            //if (eventLevel.ordinal() > mLogLevel.ordinal())
            //return;

            if (mLogListener != null)
            {
                if (mLogListener.onLogEvent(this, event1, eventLevel))
                {
                    return;
                }
            }
        }

        private void fireCallback(CallbackType callbackType, Exception ex)
        {
            // Check if listener is set.
            if (mListener != null)
            {

                switch (callbackType)
                {
                    case MASTNativeAd.CallbackType.NativeReceived:
                        mListener.onNativeAdReceived(this);
                        break;
                    case MASTNativeAd.CallbackType.NativeFailed:
                        mListener.onNativeAdFailed(this, ex);
                        break;
                    case MASTNativeAd.CallbackType.NativeAdClicked:
                        mListener.onNativeAdClicked(this);
                        break;
                }
            }
        }

        // Listeners for AdRequest.Listener start
        /*
         * Deprecating these methods as these methods are public and may be
         * accidentally called by the user causing unexpected behavior.
         * 
         * The access modifier will be changed to protected or default in the future
         * so that the user will not be able to see these methods and will not be
         * able to invoke these.
         */
        private void adRequestFailed(AdRequest request, Exception exception)
        {
            logEvent("Ad request failed: " + exception, LogLevel.Error);

            fireCallback(CallbackType.NativeFailed, exception);
        }

        private void adRequestError(AdRequest request, string errorCode, string errorMessage)
        {

            if (request != mAdRequest)
                return;

            fireCallback(CallbackType.NativeFailed, new Exception(errorMessage));

            LogLevel logLevel = LogLevel.Error;
            if ("404".Equals(errorCode))
            {
                logLevel = LogLevel.Debug;
            }

            logEvent("Error response from server.  Error code: " + errorCode
                    + ". Error message: " + errorMessage, logLevel);

        }

        private void adRequestCompleted(AdRequest adRequest, AdDescriptor adDescriptor)
        {
            if (adRequest != mAdRequest)
                return;

            if (adDescriptor != null && adDescriptor is NativeAdDescriptor)
            {
                mNativeAdDescriptor = (NativeAdDescriptor)adDescriptor;

                // If it is the native response, then pass the native response
                // Set all the properties for native ad
                fireCallback(CallbackType.NativeReceived, null);
            }
            else
            {
                fireCallback(CallbackType.NativeFailed, new Exception("Incorrect response received"));
            }
        }

        private void adRequestServed(AdRequest request, System.Net.HttpWebResponse response)
        {
            System.IO.Stream responseStream = response.GetResponseStream();

            AdDescriptor adDescriptor = AdDescriptor.parseNativeResponse(responseStream);

            if (request != mAdRequest)
                return;

            if (adDescriptor != null && adDescriptor is NativeAdDescriptor)
            {
                mNativeAdDescriptor = (NativeAdDescriptor)adDescriptor;

                // If it is the native response, then pass the native response
                // Set all the properties for native ad
                fireCallback(CallbackType.NativeReceived, null);
            }
            else
            {
                fireCallback(CallbackType.NativeFailed, new Exception("Incorrect response received"));
            }
        }

        private String generateNativeAssetRequestJson()
        {

            String nativeRequestPostData = MASTNativeAdConstants.REQUEST_NATIVE_EQ_WRAPPER;

            JsonObject nativeObj = new JsonObject();
            nativeObj.Add(MASTNativeAdConstants.REQUEST_VER, JsonValue.CreateStringValue(MASTNativeAdConstants.REQUEST_VER_VALUE_1));

            JsonArray assetsArray = new JsonArray();

            JsonObject assetObj;
            JsonObject titleObj;
            JsonObject imageObj;
            JsonObject dataObj;

            for (int i = 0; i < mRequestedNativeAssets.Count; i++)
            {
                AssetRequest assetRequest;

                assetRequest = mRequestedNativeAssets[i];

                if (assetRequest != null)
                {
                    assetObj = new JsonObject();
                    assetObj.Add(MASTNativeAdConstants.ID_STRING, JsonValue.CreateNumberValue(assetRequest.getAssetId()));
                    assetObj.Add(MASTNativeAdConstants.REQUEST_REQUIRED, JsonValue.CreateNumberValue((assetRequest.isRequired() ? 1 : 0)));

                    if (assetRequest is TitleAssetRequest)
                    {
                        if (((TitleAssetRequest)assetRequest).length > 0)
                        {
                            titleObj = new JsonObject();
                            titleObj.Add(MASTNativeAdConstants.REQUEST_LEN, JsonValue.CreateNumberValue(((TitleAssetRequest)assetRequest).length));
                            assetObj.Add(MASTNativeAdConstants.REQUEST_TITLE, titleObj);
                        }
                        else
                        {
                            assetObj = null;
                            //Log.w("AdRequest", "'length' parameter is mandatory for title asset");
                        }
                    }
                    else if (assetRequest is ImageAssetRequest)
                    {
                        imageObj = new JsonObject();

                        if (((ImageAssetRequest)assetRequest).getImageType() != null)
                        {
                            imageObj.Add(MASTNativeAdConstants.REQUEST_TYPE, JsonValue.CreateNumberValue(((ImageAssetRequest)assetRequest).getTypeId()));
                        }
                        if (((ImageAssetRequest)assetRequest).width > 0)
                        {
                            imageObj.Add(MASTNativeAdConstants.NATIVE_IMAGE_W, JsonValue.CreateNumberValue(((ImageAssetRequest)assetRequest).width));
                        }
                        if (((ImageAssetRequest)assetRequest).height > 0)
                        {
                            imageObj.Add(MASTNativeAdConstants.NATIVE_IMAGE_H, JsonValue.CreateNumberValue(((ImageAssetRequest)assetRequest).height));
                        }

                        assetObj.Add(MASTNativeAdConstants.REQUEST_IMG, imageObj);
                    }
                    else if (assetRequest is DataAssetRequest)
                    {
                        dataObj = new JsonObject();
                        if (((DataAssetRequest)assetRequest).dataAssetType != null)
                        {
                            dataObj.Add(MASTNativeAdConstants.REQUEST_TYPE, JsonValue.CreateNumberValue(((DataAssetRequest)assetRequest).getTypeId()));

                            if (((DataAssetRequest)assetRequest).length > 0)
                            {
                                dataObj.Add(MASTNativeAdConstants.REQUEST_LEN, JsonValue.CreateNumberValue(((DataAssetRequest)assetRequest).length));
                            }

                            assetObj.Add(MASTNativeAdConstants.REQUEST_DATA, dataObj);
                        }
                        else
                        {
                            assetObj = null;
                            //Log.w("AdRequest","'type' parameter is mandatory for data asset");
                        }
                    }
                    if (assetObj != null)
                    {
                        assetsArray.Add(assetObj);
                    }
                }
            }

            nativeObj.Add(MASTNativeAdConstants.NATIVE_ASSETS_STRING, assetsArray);

            nativeRequestPostData += nativeObj.ToString();

            return nativeRequestPostData;

        }

        public class Image
        {
            String url = null;
            int width = 0;
            int height = 0;

            public Image(String url)
            {
                this.url = url;
            }

            public Image(String url, int width, int height)
            {
                this.url = url;
                this.width = width;
                this.height = height;
            }

            public String getUrl()
            {
                return url;
            }

            int getWidth()
            {
                return width;
            }

            int getHeight()
            {
                return height;
            }

            /**
             * Parses the JSON and returns the object of {@link Image}, null
             * otherwise.
             * 
             * @param json
             * @return object of {@link Image}, else null if the parsing fails
             */
            public static Image getImage(Windows.Data.Json.JsonObject jsonImage)
            {
                Image image = null;

                if (jsonImage != null && jsonImage[MASTNativeAdConstants.RESPONSE_URL].GetString() != null)
                {
                    String url = jsonImage[MASTNativeAdConstants.RESPONSE_URL].GetString();
                    double width = jsonImage[MASTNativeAdConstants.NATIVE_IMAGE_W].GetNumber();
                    double height = jsonImage[MASTNativeAdConstants.NATIVE_IMAGE_H].GetNumber();
                    image = new Image(url, Convert.ToInt32(width), Convert.ToInt32(height));
                }

                return image;
            }
        }

    }
}
