﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    class Defaults
    {
        public static String SDK_VERSION = "4.2";

        // This is used if the WebView's value returned is empty.
        public static String USER_AGENT = "MASTAdView/" + SDK_VERSION
                + " (Windows)";

        public static int NETWORK_TIMEOUT_SECONDS = 500000;

        // 10 mins in ms
        public static int LOCATION_DETECTION_MINTIME = 10 * 60 * 1000;
        public static int LOCATION_DETECTION_MINDISTANCE = 20; // Meters

        // How much content is allowed after parsing out click url and image or text
        // content before
        // falling through and rendering as html vs. native rendering.
        public static int DESCRIPTOR_THIRD_PARTY_VALIDATOR_LENGTH = 20;

        public static String AD_NETWORK_URL = "http://ads.moceanads.com/ad";

        // Default injection HTML rich media ads.
        // IMPORTANT: This string have specific format specifiers (%s).
        // Improper modification to this string can cause ad rendering failures.
        public static String RICHMEDIA_FORMAT = "<html><head><meta name=\"viewport\" content=\"user-scalable=0\"/><style>body{margin:0;padding:0;}</style><script type=\"text/javascript\">%s</script></head><body>%s</body></html>";

        // Defaults for native ad serving
        public static String NATIVE_REQUEST_COUNT = "1";
        public static String NATIVE_REQUEST_KEY = "8";
        public static String NATIVE_REQUEST_AD_TYPE = "8";
        public static String NATIVE_REQUEST_TEST_TRUE = "1";
        public static String ENCODING_UTF_8 = "UTF-8";
    }
}
