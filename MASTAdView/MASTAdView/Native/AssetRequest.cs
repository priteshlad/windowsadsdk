﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class AssetRequest
    {
        /** Asset Id */
        public int assetId;

        /** Flag to check if asset is required or optional */
        public bool isAssetRequired = false;

        public int getAssetId()
        {
            return assetId;
        }

        public void setAssetId(int assetId)
        {
            this.assetId = assetId;
        }

        public bool isRequired()
        {
            return isAssetRequired;
        }

        /**
         * Set if asset is required or optional.
         * <p>
         * Default: false (optional)
         * 
         * @param isRequired
         *            : Set true for required or false for optional.
         */
        public void setRequired(bool isRequired)
        {
            this.isAssetRequired = isRequired;
        }
    }
}
