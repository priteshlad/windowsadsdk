﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class TitleAssetResponse : AssetResponse
    {
        /** Title text received */
        private String titleText;

        public String getTitleText()
        {
            return titleText;
        }

        public void setTitleText(String titleText)
        {
            this.titleText = titleText;
        }
    }
}
