﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class DataAssetRequest : AssetRequest
    {
        /** Length for DataAsset */
        public int length;

        /** Data Asset Type */
        public DataAssetTypes dataAssetType;

        public int getLength()
        {
            return length;
        }

        public void setLength(int length)
        {
            this.length = length;
        }

        public DataAssetTypes getDataAssetType()
        {
            return dataAssetType;
        }

        public void setDataAssetType(DataAssetTypes dataAssetType)
        {
            this.dataAssetType = dataAssetType;
        }

        public int getTypeId()
        {
            if (dataAssetType == DataAssetTypes.sponsored)
                return 1;
            else if (dataAssetType == DataAssetTypes.desc)
                return 2;
            else if (dataAssetType == DataAssetTypes.rating)
                return 3;
            else if (dataAssetType == DataAssetTypes.likes)
                return 4;
            else if (dataAssetType == DataAssetTypes.downloads)
                return 5;
            else if (dataAssetType == DataAssetTypes.price)
                return 6;
            else if (dataAssetType == DataAssetTypes.saleprice)
                return 7;
            else if (dataAssetType == DataAssetTypes.phone)
                return 8;
            else if (dataAssetType == DataAssetTypes.address)
                return 9;
            else if (dataAssetType == DataAssetTypes.desc2)
                return 10;
            else if (dataAssetType == DataAssetTypes.displayurl)
                return 11;
            else if (dataAssetType == DataAssetTypes.ctatext)
                return 12;
            else if (dataAssetType == DataAssetTypes.OTHER)
                return 501;
            else
                return -1;
        }
    }
}
