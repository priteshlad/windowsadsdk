﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class DataAssetResponse : AssetResponse
    {
        /** Data Asset Type */
        public DataAssetTypes dataAssetType;

        /** Value of data asset received */
        public String value;

        public DataAssetTypes getDataAssetType()
        {
            return dataAssetType;
        }

        public void setDataAssetType(DataAssetTypes dataAssetType)
        {
            this.dataAssetType = dataAssetType;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }
    }
}
