﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class TitleAssetRequest : AssetRequest
    {
        /** Character length of title asset */
        public int length;

        public int getLength()
        {
            return length;
        }

        public void setLength(int length)
        {
            this.length = length;
        }
    }
}
