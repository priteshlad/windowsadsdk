﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace MASTNativeAdView
{
    public class ImageDownloader
    {
        private System.Net.HttpWebRequest webRequest = null;
        private String requestURL;
        private Image imageView;

        public ImageDownloader(String requestURL,Image imageView)
        {
            this.requestURL = requestURL;
            this.imageView = imageView;

            CreateGetRequest();
        }

        public void CreateGetRequest()
        {
            this.webRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(this.requestURL);

            IAsyncResult result = this.webRequest.BeginGetResponse(new AsyncCallback(OnImageReceived), null);
        }

        public async static Task loadImage(String requestURL,Image imageView)
        {
            new ImageDownloader(requestURL,imageView);
        }

        private async void OnImageReceived(IAsyncResult ar)
        {
            try
            {
                System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)this.webRequest.EndGetResponse(ar);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    // Get Image Stream
                    Stream responseStream = response.GetResponseStream();

                    // Convert Image Stream to Memory Stream
                    var memStream = new MemoryStream();
                    await responseStream.CopyToAsync(memStream);
                    memStream.Position = 0;

                    // Sets the Source of Image in UI Thread
                    await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                        () =>
                        {
                            // Removing Source
                            imageView.Source = null;
                            var bitmap = new BitmapImage();
                            bitmap.SetSource(memStream.AsRandomAccessStream());
                            imageView.Source = bitmap;
                        });
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
