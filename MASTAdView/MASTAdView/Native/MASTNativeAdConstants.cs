﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    class MASTNativeAdConstants
    {
        public static String DEFAULTED_EXCREATIVES = "excreatives";
        public static String DEFAULTED_PUBMATIC_EXFEEDS = "pubmatic_exfeeds";
        public static String TELEPHONY_MCC = "mcc";
        public static String TELEPHONY_MNC = "mnc";
        public static String REQUESTPARAM_UA = "ua";
        public static String REQUESTPARAM_SDK_VERSION = "version";
        public static String REQUESTPARAM_COUNT = "count";
        public static String REQUESTPARAM_KEY = "key";
        public static String REQUESTPARAM_TYPE = "type";
        public static String REQUESTPARAM_ZONE = "zone";
        public static String REQUESTPARAM_TEST = "test";
        public static String REQUESTPARAM_LATITUDE = "lat";
        public static String REQUESTPARAM_LONGITUDE = "long";
        public static String REQUEST_HEADER_USER_AGENT = "User-Agent";
        public static String REQUEST_HEADER_CONNECTION = "Connection";
        public static String REQUEST_HEADER_CONNECTION_VALUE_CLOSE = "close";
        public static String REQUEST_HEADER_CONTENT_TYPE = "Content-Type";
        public static String REQUEST_HEADER_CONTENT_TYPE_VALUE = "application/x-www-form-urlencoded;charset=UTF-8";

        public static String REQUEST_NATIVE_EQ_WRAPPER = "native=";
        public static String REQUEST_VER = "ver";
        public static String REQUEST_VER_VALUE_1 = "1";
        public static String REQUEST_REQUIRED = "required";
        public static String REQUEST_LEN = "len";
        public static String REQUEST_TITLE = "title";
        public static String REQUEST_TYPE = "type";
        public static String REQUEST_IMG = "img";
        public static String REQUEST_DATA = "data";
        public static String NATIVE_ASSETS_STRING = "assets";
        public static String NATIVE_IMAGE_W = "w";
        public static String NATIVE_IMAGE_H = "h";

        public static String NEWLINE = "\n";
        public static String QUESTIONMARK = "?";
        public static String AMPERSAND = "&";
        public static String EQUAL = "=";

        public static String RESPONSE_HEADER_CONTENT_TYPE_JSON = "application/json";

        public static String RESPONSE_ADS = "ads";
        public static String RESPONSE_ERROR = "error";
        public static String RESPONSE_ERROR_CODE = "code";
        public static String RESPONSE_TYPE = "type";
        public static String RESPONSE_SUBTYPE = "subtype";
        public static String RESPONSE_CREATIVEID = "creativeid";
        public static String RESPONSE_FEEDID = "feedid";
        public static String RESPONSE_MEDIATION = "mediation";
        public static String RESPONSE_MEDIATION_NAME = "name";
        public static String RESPONSE_MEDIATION_SOURCE = "source";
        public static String ID_STRING = "id";
        public static String RESPONSE_MEDIATION_DATA = "data";
        public static String RESPONSE_MEDIATION_ADID = "adid";
        public static String RESPONSE_IMPTRACKERS = "imptrackers";
        public static String RESPONSE_JSTRACKER = "jstracker";
        public static String RESPONSE_CLICKTRACKERS = "clicktrackers";
        public static String RESPONSE_VER = "ver";
        public static String RESPONSE_NATIVE_STRING = "native";
        public static String RESPONSE_THIRDPARTY_STRING = "thirdparty";
        public static String RESPONSE_DIRECT_STRING = "direct";
        public static String RESPONSE_LINK = "link";
        public static String RESPONSE_URL = "url";
        public static String RESPONSE_FALLBACK = "fallback";

        public static String RESPONSE_IMG = "img";
        public static String RESPONSE_TITLE = "title";
        public static String RESPONSE_TEXT = "text";
        public static String RESPONSE_DATA = "data";
        public static String RESPONSE_VALUE = "value";
    }
}
