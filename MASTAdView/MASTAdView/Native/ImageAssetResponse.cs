﻿using MASTNativeAdView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class ImageAssetResponse : AssetResponse
    {
        /** Image Asset */
        public MASTNativeAd.Image image;

        /** Image Type (Optional in response) */
        public ImageAssetTypes imageType;

        public MASTNativeAd.Image getImage()
        {
            return image;
        }

        public void setImage(MASTNativeAd.Image image)
        {
            this.image = image;
        }

        /**
         * Image type is only available in case of mediation response
         * 
         * @return Type of image asset
         */
        public ImageAssetTypes getImageType()
        {
            return imageType;
        }

        public void setImageType(ImageAssetTypes imageType)
        {
            this.imageType = imageType;
        }

        public int getTypeId()
        {
            if (imageType == ImageAssetTypes.icon)
                return 1;
            else if (imageType == ImageAssetTypes.logo)
                return 2;
            else if (imageType == ImageAssetTypes.main)
                return 3;
            else
                return -1;
        }
    }
}
