﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASTNativeAdView
{
    public class AssetResponse
    {
        /** Asset Id */
        public int assetId;

        public int getAssetId()
        {
            return assetId;
        }

        public void setAssetId(int assetId)
        {
            this.assetId = assetId;
        }

    }
}
